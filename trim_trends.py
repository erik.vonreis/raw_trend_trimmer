"""raw trend trimmer
"""

import sys
import gpstime
from argparse import ArgumentParser
import logging
import os
import struct
import os.path as path
from typing import List, BinaryIO
from dataclasses import dataclass
from tempfile import TemporaryFile
from shutil import copyfileobj, copyfile
from threading import Thread

loglevel = os.environ.get('LOGLEVEL', 'WARNING').upper()
logging.basicConfig(level=loglevel)


parser = ArgumentParser(
    prog='trend_trimmer',
    description='trim raw trends'
)

parser.add_argument("trend_path",
                    help='path to raw minute trends')
parser.add_argument("retain_days", type=int, nargs='?', default=366,
                    help='number of minutes to retain')
parser.add_argument("--force", '-f', action='store_true', help="turn errors into warnings")

args = parser.parse_args()

retain_days = args.retain_days
retain_sec = retain_days * 24 * 3600
now_gps = gpstime.gpsnow()
threshhold_gps = now_gps - retain_sec
trend_path = args.trend_path
force = args.force

logging.info(f"trend path = {trend_path}")
logging.info(f"retain days = {retain_days}")
logging.info(f"retain seconds = {retain_sec}")
logging.info(f"retain seconds = {retain_sec}")
logging.info(f"gps now = {now_gps}")
logging.info(f"threshold (gps) = {threshhold_gps}")

BLOCK_SIZE = 40

@dataclass
class RawTrendBlock:
    timestamp_gps: int
    min: float
    max: float
    n: int
    rms: float
    mean: float

def read_block(f: BinaryIO, n: int) -> RawTrendBlock:
    global BLOCK_SIZE
    raw_trend_block = "<IddIdd"
    size = struct.calcsize(raw_trend_block)

    # size of raw trend structure must be 40 bytes
    assert (size == BLOCK_SIZE)
    offset = size * n

    f.seek(offset)
    buff = f.read(size)
    tup = struct.unpack(raw_trend_block, buff)
    return RawTrendBlock(*tup)


def get_num_blocks(f: BinaryIO) -> (int, bool):
    """
    Get the number of blocks in the file

    :param f:
    :return: return the number of blocks, also return true if the file is uneven and the end needs to be trimmed
    """
    global BLOCK_SIZE
    old_pos = f.tell()
    f.seek(0,2)
    end_size = f.tell()
    f.seek(old_pos)
    logging.debug(f"size={end_size}")
    if end_size % BLOCK_SIZE != 0:
        logging.warning(f"file size is not a multiple of block size, which is {BLOCK_SIZE} bytes")
        trim_end = True
    else:
        trim_end = False
    return end_size // BLOCK_SIZE, trim_end


def calc_jump(start_gps: float, target_gps: float) -> int:
    return int((target_gps - start_gps) // 60)


def trim(filename: str, cut_blocks: int) -> None:
    global BLOCK_SIZE
    temp_name = f"{filename}.trim.tmp"
    logging.info(f"trimming {cut_blocks} records from {filename}")
    cut_bytes = cut_blocks * BLOCK_SIZE
    with open(temp_name, "wb") as to_f:
        with open(filename, "rb") as from_f:
            from_f.seek(0,2)
            end_size = from_f.tell()
            extra_bytes = end_size % BLOCK_SIZE
            from_f.seek(cut_bytes)
            copyfileobj(from_f, to_f)
            if extra_bytes > 0:
                logging.info(f"trimming {extra_bytes} from end of file '{filename}'.")
                to_f.seek(-extra_bytes, 2)
                to_f.truncate()
    # copy temporary over old file
    copyfile(temp_name, filename)
    os.remove(temp_name)


def trim_decreasing_time(filename: str, beginning_bn: int, end_bn: int):
    """
    Trim off any records from before a backwards transition in time.

    :param filename:
    :param beginning:
    :param end:
    :return:
    """
    with open(filename, "rb") as f:
        top_bn = end_bn
        bottom_bn = beginning_bn

        assert(bottom_bn < top_bn)

        top_block = read_block(f, top_bn)
        bottom_block = read_block(f, bottom_bn)

        assert(bottom_block.timestamp_gps >= top_block.timestamp_gps)

        while top_bn - bottom_bn > 1:
            check_bn = (top_bn + bottom_bn) // 2
            check_block = read_block(f, check_bn)
            if check_block.timestamp_gps <= bottom_block.timestamp_gps:
                bottom_bn = check_bn
                bottom_block = check_block
            else:
                top_bn = check_bn

    trim(filename, top_bn)


def trim_trend_file(filename: str, threshold_gps: float):
    """
    Trim a trend file so that it only has records after threshold_gps
    :param filename:
    :param threshold_gps:
    :return:
    """
    with open(filename, "rb") as f:
        logging.debug(f"{filename}: threshold = {threshold_gps}")
        num_blocks, trim_end = get_num_blocks(f)
        logging.info(f"num blocks = {num_blocks}")
        if num_blocks == 0:
            return
        top_block_num = num_blocks
        bottom_block_num = 0
        bottom_block = read_block(f, bottom_block_num)
        logging.debug(f"first block {bottom_block}")

        check_block_num = bottom_block_num + calc_jump(bottom_block.timestamp_gps, threshold_gps)
        if check_block_num <= bottom_block_num:
            top_block_num = check_block_num + 1

        out_of_order = None

        half = False
        while bottom_block.timestamp_gps < threshold_gps and top_block_num - bottom_block_num > 1:
            # if there's a break, resort to binary search
            if check_block_num >= top_block_num or check_block_num <= bottom_block_num:
                check_block_num = (top_block_num + bottom_block_num) // 2

            logging.debug(f"{filename}: check = {check_block_num} bottom = {bottom_block_num} top = {top_block_num}")
            check_block = read_block(f, check_block_num)
            logging.debug(f"{filename}: check block num = {check_block_num} gps = {check_block.timestamp_gps}")
            if check_block.timestamp_gps <= bottom_block.timestamp_gps:
                logging.error(f"timestamps aren't uniformly increasing in {filename}")
                out_of_order = (bottom_block_num, check_block_num)
            if check_block.timestamp_gps >= threshold_gps:
                logging.debug(f"{filename}: check is high")
                top_block_num = check_block_num

            else:
                logging.debug(f"{filename}: check is low")
                bottom_block_num = check_block_num
                bottom_block = check_block
                check_block_num = check_block_num + calc_jump(check_block.timestamp_gps, threshold_gps)
            jump = calc_jump(check_block.timestamp_gps, threshold_gps)
            check_block_num = check_block_num + jump

    if out_of_order is not None:
        trim_decreasing_time(filename, *out_of_order)
        return trim_trend_file(filename, threshold_gps)
    elif bottom_block.timestamp_gps > threshold_gps:
        cut_before = bottom_block_num
    else:
        cut_before = top_block_num
    if cut_before >= num_blocks:
        logging.info(f"{filename}: all blocks are earlier than threshold, delete file")
        os.remove(filename)
    elif cut_before > 0:
        logging.info(f"{filename}: trim first {cut_before} blocks")
        # copy to temporary
        trim(filename, cut_before)
    elif trim_end:
        logging.info(f"{filename}: incomplete block found.  Trimming end.")
        trim(filename, 0)
    else:
        logging.info(f"{filename}: all blocks are later than threshold, do nothing")


def check_trend_path(dir_names: List[str], fnames: List[str]) -> bool:
    """
    Returns true if dir_names look like the dir_names in a trend path
    :param dir_names:
    :param fnames:
    :return:
    """
    is_good = True
    good_chars = "0123456789abcdef"
    if len(fnames) > 0:
        logging.warning("base raw trend directories should not have any files in them, but this one does")
        is_good = False
    if len(dir_names) <= 0:
        logging.warning("base raw trends directories should have some subdirectories, but this one doesn't")
        is_good = False
    for dir_name in dir_names:
        if len(dir_name) > 2 or not all([c in good_chars for c in dir_name]):
            logging.warning(f"subdirectory '{dir_name}' found, but all trend subdirectories are "
                            f"one or two digit lower-case hex numbers")
            is_good = False
    return is_good

if not path.isdir(trend_path):
    logging.error(f"{trend_path} is not a directory")

for dir_path, dir_names, filenames in os.walk(trend_path):
    if dir_path == trend_path:
        if not check_trend_path(dir_names, filenames):
            msg = "trend_path may not be a raw trends directory"
            if force:
                logging.warning(msg)
            else:
                logging.error(msg)
                sys.exit(1)
    else:
        for filename in filenames:
            file_path = path.join(dir_path, filename)
            t = Thread(target=trim_trend_file, args=[file_path], kwargs={"threshold_gps": threshhold_gps})
            #trim_trend_file(file_path, threshold_gps=threshhold_gps)
            t.start()
